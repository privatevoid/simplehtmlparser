﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyHtmlParse
{
    interface IParser
    {
        void SetDocument(string document);

        IEnumerable<Found> GetValues(string tagName);

        IEnumerable<Found> GetAttributesValues(string attribute);
    }
}
