﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace EasyHtmlParse
{
    public class Parser
    {
        public IEnumerable<Found> GetAttributesValues(string attribute)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Found> GetValues(string html)
        {
            return GetValues(html, null);
        }

        public IEnumerable<Found> GetValues(string html, string tag)
        {
            var tagName = tag == null ? Regex.Match(html, @"<\w+").Value.Replace("<", string.Empty) : tag;

            List<Found> founds = new List<Found>();

            string pattern = $@"<{tagName}.*\/{tagName}>?";
            string valPattern = $@"(?<=<{tagName}).*(?=<\/{tagName}>)";

            Regex reg = new Regex(pattern, RegexOptions.Singleline);
            Regex valreg = new Regex(valPattern, RegexOptions.Singleline);

            var m = (from t in reg.Matches(html).Cast<Match>()
                     let val = valreg.Match(t.Value)
                     select new Found(t.Value, val.Value.Substring(val.Value.IndexOf('>') + 1), tagName)).ToArray();

            founds.AddRange(m);

            foreach (var found in m)
            {
                founds.AddRange(GetValues(found.Value, tag));
            }

            return founds;
        }

        public void SetDocument(string document)
        {
           
        }
    }
}
