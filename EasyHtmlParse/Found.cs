﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EasyHtmlParse
{
    public class Found
    {

        internal string Element { get; private set; }

        public string Value { get; private set; }

        public string Tag { get; private set; }

        public IEnumerable<HtmlAttribute> Attributes { get; private set; }

        public Found(string element, string value, string tagName)
        {
            Element = element;
            Value = value;


            Attributes = GetNonValueAttributes().Union(GetValueAttributes());
        }


        private IEnumerable<HtmlAttribute> GetNonValueAttributes()
        {
            Regex attrNameReg = new Regex(@"\s([^""']*]?)\s", RegexOptions.Singleline);
            var firstTag = Element.Substring(0, Element.IndexOf('>'));

            return from t in attrNameReg.Matches(firstTag).Cast<Match>()
                   let attrName = attrNameReg.Match(t.Value).Value.Trim()
                   select new HtmlAttribute() { Name = attrName };

        }

        private IEnumerable<HtmlAttribute> GetValueAttributes()
        {
            string attrPattern = @"((\S+)=[""']?((?:.(?![""']?\s + (?:\S+)=|[>""']))+.)[""']?)";

            Regex attrNameReg = new Regex(@"[^=]+", RegexOptions.Singleline);
            Regex attrValueReg = new Regex(@"("".+"")|('.+')", RegexOptions.Singleline);
            Regex attrReg = new Regex(attrPattern, RegexOptions.Singleline);

            var firstTag = Element.Substring(0, Element.IndexOf('>'));

            return from t in attrReg.Matches(firstTag).Cast<Match>()
                   let attrName = attrNameReg.Match(t.Value).Value
                   let attrValue = attrValueReg.Match(t.Value).Value.Replace("'", string.Empty).Replace("\"", string.Empty)
                   select new HtmlAttribute() { Name = attrName, Value = attrValue };
        }

    }
    public struct HtmlAttribute
    {
        public string Name;
        public string Value;
    }
}
