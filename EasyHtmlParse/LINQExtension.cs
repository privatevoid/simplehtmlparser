﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyHtmlParse
{
    public static class LINQExtension
    {
        public static IEnumerable<Found> WithAttributes(this IEnumerable<Found> founds, string attributeName, string attributeValue)
        {

            var filtered = from t in founds
                           let a = t.Attributes.Where(x => x.Name == attributeName && x.Value == attributeValue)
                           where a.Count() > 0
                           select t;

            return filtered;
        }

        public static IEnumerable<Found> WithAttributes(this IEnumerable<Found> founds, string attributeName)
        {

            var filtered = from t in founds
                           let a = t.Attributes.Where(x => x.Name == attributeName)
                           where a.Count() > 0
                           select t;

            return filtered;
        }

        public static IEnumerable<Found> WhereValueContains(this IEnumerable<Found> founds, string text)
        {
            var filtered = from t in founds
                           where t.Value.Contains(text)
                           select t;
            return filtered;
        }

        public static IEnumerable<Found> WhereValueEquals(this IEnumerable<Found> founds, string text)
        {
            var filtered = from t in founds
                           where t.Value.Equals(text)
                           select t;
            return filtered;
        }
    }
}
