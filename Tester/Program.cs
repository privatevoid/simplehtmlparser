﻿using EasyHtmlParse;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser p = new Parser();

            var testHtml = File.ReadAllText(Path.Combine(Environment.CurrentDirectory, @"TestHtml.html"));

            Console.WriteLine(
                p.GetValues(testHtml, "button").WithAttributes("class", "btn btn-default").FirstOrDefault().Value
                );

            Console.Read();
        }
    }
}
